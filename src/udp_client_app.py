#!/usr/bin/python

import client
import sys
import time
import os

file_name = 'EVE_Online_Installer_980995.msi'
if len(sys.argv) >= 4:
    file_name = sys.argv[3]

port = 27016
if len(sys.argv) >= 3:
    port = int(sys.argv[2])

host = 'localhost'
if len(sys.argv) >= 2:
    host = sys.argv[1]

file_size = os.stat('./sent/'+file_name).st_size
print('Transferring %s (~%s MiB)...' % (file_name, file_size / 1024 / 1024))
start = time.time() 
client = client.FileTransferClientUdp(host, port, file_name, 8192, 128)
try:
    client.start_loop()
except:
    client.perform_cleanup()
    raise

end = time.time()
print('Done! Transfer time is %ss. \nEst. transfer speed is about %s Mbps.' % ((end - start), (file_size / 1024 / 1024 * 8) / (end - start)))
