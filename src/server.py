import socket
import sys
import struct
import select

class FileTransferServer:
    def __init__(self, host, port, buffer_size):
        """Builds the FileTransferServer instance.
        
        Keyword arguments:
        host -- hostname to host the server socket on
        port -- port to host the server socket on
        buffer_size -- amount of data to read in one select call

        """

        self.__srv_addr = (host, port)
        self.__buffer_size = buffer_size
        self.__udp_max_buffer_size = 8208
        self.__udp_data_packet_size = 8192
        self.__udp_chunks_before_validation = 128
        self.__srv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
       # self.__srv_socket.setsockopt(socket.SOL_SOCKET, socket.SO_OOBINLINE, False)
        self.__srv_udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__srv_socket.bind(self.__srv_addr)
        self.__srv_udp_socket.bind((host, port + 1))
        self.__init_select_lists()
        self.__client_info = {}
        self.__udp_clients = {}
        self.__uninitialized_connections = {}
        self.__packer = struct.Struct('i L 128p')
        self.__oob_packer = struct.Struct('i L L')
        self.__udp_packer = struct.Struct('i L 8192s')
        self.__udp_validity_packer = struct.Struct('i %sl' % self.__udp_chunks_before_validation)
        self.__next_free_id = 0
        self.__acknowledge_packet_size = 144
        self.__udp_init_packet_size = 144
        self.__oob_packet_size = 24

    def __init_select_lists(self):
        self.__readable_sockets = [ self.__srv_socket, self.__srv_udp_socket ]
        self.__writeable_sockets = [ ]
        self.__exceptional_sockets = [ ]

    def __process_new_connection(self):
        connection_socket, client_addr = self.__srv_socket.accept()
        self.__readable_sockets.append(connection_socket)
        self.__exceptional_sockets.append(connection_socket)
        self.__uninitialized_connections[connection_socket] = { 'init_buffer': '' }
        print('Connection accepted: %s:%s' % (client_addr[0], str(client_addr[1])))

    def __get_client_by_socket(self, socket):
        return self.__client_info[[key for key, value in self.__client_info.iteritems() if value['socket'] == socket][0]] 

    def __process_client_disconnect(self, client_socket):
        print('Closing connection with %s:%s' % (client_socket.getpeername()[0], client_socket.getpeername()[1]))
        client_socket.close()
        self.__get_client_by_socket(client_socket)['file_handle'].close()
        self.__readable_sockets.remove(client_socket)
        self.__exceptional_sockets.remove(client_socket)

    def __store_new_client(self, socket, info):
        current_id = self.__next_free_id + 1
        self.__next_free_id += 1
        self.__client_info[current_id] = { 'file_name': info[2], 'offset': 0L, 'file_handle': open('./recieved/' + info[2], 'wb'), 'socket': socket }
        self.__uninitialized_connections.pop(socket)
        print('Stored new client with id=%s' % (current_id))
        self.__send_acknowledge_packet(current_id)

    def __resore_existing_client(self, socket, info):
        if info[0] in self.__client_info:
            client_info = self.__client_info[info[0]]
            client_info['socket'].close()
            client_info['socket'] = socket
            client_info['offset'] = os.stat('./recieved/' + client_info['file_name']).st_size
            client_info['file_handle'].seek(0, 2)
            self.__readable_sockets.remove(socket)
            self.__exceptional_sockets.remove(socket)
            print('Restored existing client with id=%s' % (info[0]))
            self.__send_acknowledge_packet(info[0])
        else:
            print('WARNING! A client on %s tried to empersonate an existing client... Enforcing new client_id.' % socket.getpeername())
            self.__store_new_client(socket, info)

    def __rebuild_socket_init_packet(self, socket):
        info = self.__packer.unpack(self.__uninitialized_connections[socket]['init_buffer'])
        if info[0] is -1:
            self.__store_new_client(socket, info)
        else:
            self.__restore_existing_client(socket, info)
        
    def __read_out_of_band_data(self, data_socket):
        oob_data = data_socket.recv(self.__oob_packet_size, socket.MSG_OOB)
        if oob_data:
            print('OOB data ready: %s' % (oob_data))

    def __send_acknowledge_packet(self, client_id):
        client_descriptor = self.__client_info[client_id]
        packed_info = self.__packer.pack(*(client_id, client_descriptor['offset'], client_descriptor['file_name']))
        client_descriptor['socket'].send(packed_info)

    def __initialize_connection(self, data, socket):
        self.__uninitialized_connections[socket]['init_buffer'] += data
        if len(self.__uninitialized_connections[socket]['init_buffer']) >= self.__acknowledge_packet_size:
            self.__rebuild_socket_init_packet(socket)

    def __process_incoming_data(self, data_socket):
        recieved_data = data_socket.recv(self.__buffer_size)
        if recieved_data:
            if data_socket in self.__uninitialized_connections:
                self.__initialize_connection(recieved_data, data_socket)
            else:
                self.__get_client_by_socket(data_socket)['file_handle'].write(recieved_data)
        else:
            self.__process_client_disconnect(data_socket)

    def __forget_recieved_indices(self, id):
        if self.__udp_clients[id]['validity_checked']:
            self.__udp_clients[id]['validity_checked'] = False
            self.__udp_clients[id]['recieved_indices'] = [ ]

    def __flush_packet_to_fs(self, packet):
        self.__udp_clients[packet[0]]['file_handle'].seek(len(packet[2]) * packet[1])
        #print('seeking to %s' % (len(packet[2]) * packet[1]))
        self.__udp_clients[packet[0]]['file_handle'].write(packet[2])

    def __process_udp_data_packet(self, data):
        unpacked = self.__udp_packer.unpack(data)
        self.__forget_recieved_indices(unpacked[0])
        self.__udp_clients[unpacked[0]]['recieved_indices'].append(unpacked[1])
        self.__flush_packet_to_fs(unpacked)

    def __process_udp_validity_packet(self, data, addr):
        unpacked = [index for index in self.__udp_validity_packer.unpack(data) if index != -1]
        client_id = unpacked[0]
        unpacked = unpacked[1:]
        lost_packets = [item for item in unpacked if item not in self.__udp_clients[client_id]['recieved_indices']]
        response = (client_id,) + tuple(lost_packets) + (self.__udp_chunks_before_validation - len(lost_packets)) * (-1, )
        packed = self.__udp_validity_packer.pack(*response)
        self.__srv_udp_socket.sendto(packed, addr)
        self.__udp_clients[client_id]['validity_checked'] = True

    def __initialize_udp_client(self, packet):
        file_handle = open('./recieved/' + packet[2], 'wb')
        file_handle.seek(packet[1] - 1)
        file_handle.write('\0')
        file_handle.close()
        print('creating file of size %s' % packet[1])
        file_handle = open('./recieved/' + packet[2], 'wb')
        self.__udp_clients[packet[0]] = { 'file_handle': file_handle, 'recieved_indices': [ ], 'validity_checked': False }

    def __process_init_packet(self, data, addr):
        init_packet = self.__packer.unpack(data)
        current_id = self.__next_free_id + 1
        self.__next_free_id += 1
        init_packet = (current_id, init_packet[1], init_packet[2])
        self.__initialize_udp_client(init_packet)
        packed_data = self.__packer.pack(*init_packet)
        self.__srv_udp_socket.sendto(packed_data, addr)

    def __process_udp_data(self, socket):
        data, addr = socket.recvfrom(self.__udp_max_buffer_size)
        if len(data) == self.__udp_max_buffer_size:
            self.__process_udp_data_packet(data)
        elif len(data) == self.__udp_init_packet_size:
            self.__process_init_packet(data, addr)
        else:
            self.__process_udp_validity_packet(data, addr)

    def __handle_inputs(self, input_sockets):
        for socket in input_sockets:
            if socket is self.__srv_socket:
                self.__process_new_connection()
            elif socket is self.__srv_udp_socket:
                self.__process_udp_data(socket)
            else:
                self.__process_incoming_data(socket)

    def __handle_oob(self, exceptions):
        for socket in exceptions:
            self.__read_out_of_band_data(socket)

    def __process_server_loop(self):
        ins, outs, exc = select.select(self.__readable_sockets, self.__writeable_sockets, self.__exceptional_sockets)
        self.__handle_inputs(ins)
        #self.__handle_oob(exc)

    def perform_cleanup(self):
        """Performs the server cleanup by closing the sockets and file descriptors currently in use.

        """
        self.__srv_socket.close()
        for socket in self.__readable_sockets:
            socket.close()
        
        for client_id, client_info in self.__client_info.iteritems():
            client_info['file_handle'].close()

    def start_loop(self, backlog):
        """Starts the server loop thus enabling it to listen for connections and perform file transfers

        Keyword arguments:
        backlog -- maximum concurrent amount of pending connections

        """
        self.__srv_socket.listen(backlog)
        while True:
            try:
                self.__process_server_loop()
            except KeyboardInterrupt:
                print('aught SIGINT. Cleaning up and shutting down...')
                self.perform_cleanup()
                break
