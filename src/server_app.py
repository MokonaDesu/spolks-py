#!/usr/bin/python

import server

server = server.FileTransferServer('localhost', 27015, 8192)
try:
    server.start_loop(1)
except:
    server.perform_cleanup()
    raise
