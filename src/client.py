import socket
import sys
import time
import struct
import os
import random

class FileTransferClientTcp:
    def __init__(self, host, port, file_to_transfer, buffer_size):
        """Builds the FileTransferClient instance

        Keyword arguments:
        host -- host of the server to upload file to
        port -- port of the server to upload file to
        file_to_transfer -- file path to transfer
        buffer_size -- amount of data to read from file and send each client loop tick

        """
        self.__buffer_size = buffer_size
        self.__server_addr = (host, port)
        self.__client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__file_size = os.stat('./sent/' + file_to_transfer).st_size
        self.__file_handle = open('./sent/' + file_to_transfer)
        self.__client_info = { 'id': -1, 'offset': 0, 'file_name': file_to_transfer }
        self.__packer = struct.Struct('i L 128p')
        self.__oob_packer = struct.Struct('i L L')
        self.__handshake_packet_size = 144
        self.__times_sent = 0

    def __handshake(self):        
        current_info = self.__packer.pack(*(self.__client_info['id'], self.__client_info['offset'], self.__client_info['file_name']))
        self.__client_socket.send(current_info)
        acknowledged_info = self.__client_socket.recv(self.__handshake_packet_size)
        unpacked = self.__packer.unpack(acknowledged_info)
        self.__client_info['id'] = unpacked[0]
        self.__client_info['offset'] = unpacked[1]
        self.__file_handle.seek(self.__client_info['offset'])
        self.__client_info['file_name'] = unpacked[2]
        
    def __connect(self):
        while True:
            try:
                self.__client_socket.connect(self.__server_addr)
                print('Connected!')
                self.__handshake()
                break
            except socket.error:
                time.sleep(1)
                print('Couldn\'t connect... attempting again')
                
    def __send_oob_msg_status(self):
        data_to_send = self.__oob_packer.pack(self.__client_info['id'], self.__client_info['offset'], self.__file_size)
        self.__client_socket.send(data_to_send, socket.MSG_OOB)

    def __count_oob_msg_ticks(self):
        self.__times_sent += 1
        if self.__times_sent == 5000:
            print('%s%% sent...' % (int(100 * self.__client_info['offset'] / self.__file_size)))
            self.__times_sent = 0
            #self.__send_oob_msg_status()

    def __process_client_loop(self):
        self.__connect()
        data_to_send = self.__file_handle.read(self.__buffer_size)
        while data_to_send:
            self.__count_oob_msg_ticks()
            bytes_sent = self.__client_socket.send(data_to_send)
            self.__client_info['offset'] += bytes_sent
            data_to_send = self.__file_handle.read(self.__buffer_size)  
            
    def start_loop(self):
        """Starts the client loop this initiating the connection and sending process

        """
        try:
            self.__process_client_loop()
        except KeyboardInterrupt:
            print('aught SIGINT. Cleaning up and shutting down...')
            self.perform_cleanup()
    
    def perform_cleanup(self):
        """Performs the client cleanup by closing the client socket and currently used file descriptor

        """
        self.__file_handle.close()
        self.__client_socket.close()

class FileTransferClientUdp:
    def __init__(self, host, port, file_to_transfer, buffer_size, chunks_before_validation):
        self.__buffer_size = buffer_size
        self.__chunks_before_validation = chunks_before_validation
        self.__packets_sent = 0
        self.__next_chunk = 0
        self.__packet_loss_chance = 0.1
        self.__server_addr = (host, port)
        self.__client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__client_socket.settimeout(0.1)
        self.__file_name = file_to_transfer
        self.__file_size = os.stat('./sent/' + file_to_transfer).st_size
        self.__file_handle = open('./sent/' + file_to_transfer)
        self.__packer = struct.Struct('i L 8192s')
        self.__validity_packer = struct.Struct('i %sl' % (self.__chunks_before_validation))
        self.__init_packer = struct.Struct('i L 128p')
        self.__chunks_to_resend = [ ]
        self.__sent_chunks = [ ]

    def __send_packet_to_server(self, data):
        if random.uniform(0, 1) > self.__packet_loss_chance:
            self.__client_socket.sendto(data, self.__server_addr)
        
    def __send_file_chunk(self, chunk_index):
        self.__file_handle.seek(chunk_index * self.__buffer_size)
        data_to_send = self.__file_handle.read(self.__buffer_size)
        packed_message = self.__packer.pack(self.__client_id, chunk_index, data_to_send)
        self.__sent_chunks.append(chunk_index)
        self.__send_packet_to_server(packed_message)

    def __await_validity_packet(self):
        while True:
            try:
                padded_sent_chunks = self.__sent_chunks + (self.__chunks_before_validation - len(self.__sent_chunks)) * [-1]
                validity_check_request = self.__validity_packer.pack(self.__client_id, *tuple(padded_sent_chunks))
                self.__send_packet_to_server(validity_check_request)
                validity_check_response, addr = self.__client_socket.recvfrom(len(validity_check_request))
                packets_to_resend = [index for index in self.__validity_packer.unpack(validity_check_response)[1:] if index != -1]
                self.__chunks_to_resend = [packet_index for packet_index in packets_to_resend if packet_index != -1]
                self.__sent_chunks = [ ]
                break
            except socket.timeout:
                pass

    def __get_next_chunk_index(self):
        if self.__chunks_to_resend:
            return self.__chunks_to_resend.pop()
        else:
            self.__next_chunk += 1
            return self.__next_chunk - 1

    def __file_chunk_in_bounds(self, chunk_index):
        max_chunk_size = int((self.__file_size / self.__buffer_size) + 0.5)
        return chunk_index <= max_chunk_size

    def __send_init_packet(self):
        while True:
            try:
                init_data = self.__init_packer.pack(-1, self.__file_size, self.__file_name)
                self.__send_packet_to_server(init_data)
                init_response, addr = self.__client_socket.recvfrom(len(init_data))
                init_response = self.__init_packer.unpack(init_response)
                self.__client_id = init_response[0]
                break
            except socket.timeout:
                pass

    def __flush_remaining_data(self):
        self.__await_validity_packet()
        print('flushing remaining %s packets...' % len(self.__chunks_to_resend))
        while self.__chunks_to_resend:
            for chunk in self.__chunks_to_resend:
                self.__send_file_chunk(chunk)
            self.__await_validity_packet()
        

    def __process_client_loop(self):
        self.__send_init_packet()
        while True:
            if len(self.__sent_chunks) == self.__chunks_before_validation:
                self.__await_validity_packet()
            else:
                next_chunk = self.__get_next_chunk_index()
                if self.__file_chunk_in_bounds(next_chunk):
                    self.__send_file_chunk(next_chunk)
                else:
                    break
        self.__flush_remaining_data()
    

    def start_loop(self):
        """Starts the client loop this initiating the connection and sending process

        """
        try:
            self.__process_client_loop()
        except KeyboardInterrupt:
            print('aught SIGINT. Cleaning up and shutting down...')
            self.perform_cleanup()

    def perform_cleanup(self):
        """Performs the client cleanup by closing the client socket and currently used file descriptor

        """
        self.__file_handle.close()
        self.__client_socket.close()
