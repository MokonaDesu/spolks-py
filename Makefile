server:
	python ./src/server_app.py

client:
	python ./src/client_app.py

client_udp:
	python ./src/udp_client_app.py
